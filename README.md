# voxl-gphoto2

Application to get frames from camera controlled via libgphoto2 and serve them up via MPA.

dependencies:
* libmodal_pipe
* system image 3.1.0+

This README covers building this package. The voxl-gphoto2 user manual is
located [here](https://docs.modalai.com/voxl-gphoto2/)


## Build Instructions

1) prerequisite: voxl-emulator docker image

Follow the instructions here:

https://gitlab.com/voxl-public/voxl-docker

2) prerequisite: libgphoto2_2.5.26.ipk

Run the get_build_deps.sh script to download libgphoto2_2.5.26.ipk

```bash
$ ./get_build_deps.sh
```

3) Launch Docker and make sure this project directory is mounted inside the Docker.

```bash
~/git/voxl-gphoto2$ voxl-docker -i voxl-emulator
```

4) Install dependencies inside the docker.

```bash
./install_build_deps.sh
```

5) Compile inside the docker.

```bash
./build.sh
```

6) Make an ipk package inside the docker.

```bash
./make_package.sh

Package Name:  voxl-gphoto2
version Number:  x.x.x
ar: creating voxl-gphoto2_x.x.x.ipk

DONE
```

This will make a new voxl-gphoto2_x.x.x.ipk file in your working directory. The name and version number came from the ipk/control/control file. If you are updating the package version, edit it there.


## Deploy to VOXL

You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh.

Do this OUTSIDE of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
~/git/voxl-gphoto2$ ./install_on_voxl.sh
pushing voxl-gphoto2_x.x.x.ipk to target
searching for ADB device
adb device found
voxl-gphoto2_x.x.x.ipk: 1 file pushed. 2.1 MB/s (51392 bytes in 0.023s)
Removing package voxl-gphoto2 from root...
Installing voxl-gphoto2 (x.x.x) on root.
Configuring voxl-gphoto2

Done installing voxl-gphoto2
```
