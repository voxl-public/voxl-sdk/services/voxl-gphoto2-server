/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/app/gstappsrc.h>
#include <glib-object.h>
#include <modal_pipe_server.h>
#include "context.h"
#include "input.h"
#include "output.h"
#include "pipeline.h"

// This is the main data structure for the application. It is passed / shared
// with other modules as needed.
static context_data context;

// Used to capture ctrl-c signal to allow graceful exit
void intHandler(int dummy) {
    printf("Got SIGINT, exiting\n");
    context.running = 0;
}

// This function can be registered to receive log messages from the gphoto2
// libraries
void gp_log_function(GPLogLevel level, const char *domain, const char *str, void *data) {
    printf("GPHOTO2: %s\n", str);
}

// Application help screen
void help() {
    printf("Usage: voxl-gphoto2 <options>\n");
    printf("Options:\n");
    printf("-d                Show extra debug messages.\n");
    printf("-v                Show extra frame level debug messages.\n");
    printf("-g                Show lots of gphoto2 debug messages.\n");
    printf("-h                Show help.\n");
}

//--------
//  Main
//--------
int main(int argc, char *argv[]) {
    int rc = 0;
    int opt = 0;
    pthread_t input_thread_id;

    // Parse all command line options
    while ((opt = getopt(argc, argv, "dvgf:u:h")) != -1) {
        switch (opt) {
        case 'd':
            printf("Enabling debug messages\n");
            context.debug = 1;
            break;
        case 'v':
            printf("Enabling frame debug messages\n");
            context.frame_debug = 1;
            break;
        case 'g':
            printf("Enabling libgphoto2 debug messages\n");
            context.libgphoto2_debug = 1;
            break;
        case 'h':
            help();
            return -1;
        case ':':
            fprintf(stderr, "Error - option %c needs a value\n\n", optopt);
            help();
            return -1;
        case '?':
            fprintf(stderr, "Error - unknown option: %c\n\n", optopt);
            help();
            return -1;
        }
    }

    // optind is for the extra arguments which are not parsed
    for (; optind < argc; optind++) {
        fprintf(stderr, "extra arguments: %s\n", argv[optind]);
        help();
        return -1;
    }

    // Register our gphoto2 library log handling function
    if (context.libgphoto2_debug) {
        // rc = gp_log_add_func(GP_LOG_DEBUG, gp_log_function, &context); // More debug data
        rc = gp_log_add_func(GP_LOG_DATA, gp_log_function, &context);  // Most debug data
    } else {
        rc = gp_log_add_func(GP_LOG_VERBOSE, gp_log_function, &context);
    }
    if (rc < 0) {
        fprintf(stderr, "ERROR: gp_log_add_func failed: %d %s\n", rc,
                gp_result_as_string(rc));
        return -1;
    } else {
        if (context.debug) printf("Setup gphoto2 logger\n");
    }

    // Setup the gphoto2 environment
    rc = gp_camera_new(&context.camera_ptr);
    if (rc) {
        fprintf(stderr, "ERROR: gp_camera_new failed: %s\n",
                gp_result_as_string(rc));
        return -1;
    } else {
        if (context.debug) printf("Created new camera object\n");
    }

    context.gp_context_ptr = gp_context_new();
    if (context.gp_context_ptr == NULL) {
        fprintf(stderr, "ERROR: gp_context_new failed\n");
        return -1;
    } else {
        if (context.debug) printf("Created new gphoto2 context object\n");
    }

    // This will autodetect any cameras and choose the first one
    rc = gp_camera_init(context.camera_ptr, context.gp_context_ptr);
    if (rc) {
        fprintf(stderr, "ERROR: gp_camera_init failed: %d %s\n", rc,
                gp_result_as_string(rc));
        return -1;
    } else {
        if (context.debug) printf("Initialized camera\n");
    }

    // Initialize our MPA output module
    rc = output_init(&context);
    if (rc) {
        fprintf(stderr, "ERROR: output_init failed: %d\n", rc);
        return -1;
    } else {
        if (context.debug) printf("Initialized output object\n");
    }

    // Initialize Gstreamer
    gst_init(NULL, NULL);

    // Setup our signal handler to catch ctrl-c
    signal(SIGINT, intHandler);

    pthread_mutex_init(&context.lock, NULL);

    // All systems are go...
    context.running = 1;
    context.frame_data_initialized = 0;

    // Create, configure, and start the GStreamer processing pipeline
    pipeline_init(&context);

    // Start our input frame processing thread
    pthread_create(&input_thread_id, NULL,
                   input_thread, (void*) &context);

    // Wait for a fatal error or the user exiting via ctrl-c
    while (context.running) {
        sleep(1);
    }

    // Wait for the buffer processing thread to exit
    pthread_join(input_thread_id, NULL);
    if (context.debug) printf("input_thread exited\n");

    gp_file_unref(context.camera_file);

    rc = gp_camera_exit(context.camera_ptr, context.gp_context_ptr);
    if (rc) {
        fprintf(stderr, "INFO: gp_camera_exit failed: %d %s\n", rc,
                gp_result_as_string(rc));
    }

    rc = gp_camera_unref(context.camera_ptr);
    if (rc) {
        fprintf(stderr, "INFO: gp_camera_unref failed: %s\n",
                gp_result_as_string(rc));
    }

    gp_context_unref(context.gp_context_ptr);

    // Stop the pipeline and free it
    gst_element_set_state(context.pipeline, GST_STATE_NULL);
    gst_object_unref(context.pipeline);

    // Clean up gstreamer
    gst_deinit();

    pipe_server_close_all();

    printf("voxl-gphoto2 ending\n");

    return 0;
}
