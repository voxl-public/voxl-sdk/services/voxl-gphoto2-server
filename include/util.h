/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @file util.h
 *
 * This file contains various utility functions
 */

#ifndef UTIL_H
#define UTIL_H

#include <gst/gst.h>

/**
 * @brief      Get an integer out of a caps GStructure
 *
 * @param[in]  caps     A pointer to the caps object
 * @param[in]  field    The desired field string
 * @param[in]  val      Pointer to an int for the found value
 *
 * @return     1 if an int was found, 0 if not
 */
int get_caps_int(const GstCaps* caps, const char* field, int* val);

/**
 * @brief      Verify the format contained in the caps object
 *
 * @param[in]  caps     A pointer to the caps object
 * @param[in]  format   The desired format string
 *
 * @return     1 if the format matches, 0 if not
 */
int verify_caps_format(const GstCaps* caps, const char* format);

/**
 * @brief      Print out the capabilities given a caps object
 *
 * @param[in]  caps     A pointer to the caps object
 * @param[in]  pfx      Optional prefix for printing
 */
void print_caps(const GstCaps * caps, const gchar * pfx);

/**
 * @brief      Print out the pad capabilities given the element and pad name.
 *
 * @param[in]  element     A pointer to the element containing the pad
 * @param[in]  pad_name    The name of the pad in the element (e.g. "sink", "src")
 */
void print_pad_capabilities(GstElement *element, gchar *pad_name);

#endif // UTIL_H
